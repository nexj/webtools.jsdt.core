/*******************************************************************************
 * Licensed Materials - Property of IBM
 * � Copyright IBM Corporation 2013. All Rights Reserved.
 * U.S. Government Users Restricted Rights - Use, duplication or disclosure
 * restricted by GSA ADP Schedule Contract with IBM Corp. 
 *******************************************************************************/

package org.eclipse.wst.jsdt.internal.ui.javaeditor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;

import org.eclipse.wst.jsdt.core.ISourceRange;
import org.eclipse.wst.jsdt.core.ISourceReference;
import org.eclipse.wst.jsdt.core.JavaScriptModelException;
import org.eclipse.wst.jsdt.core.dom.ASTNode;
import org.eclipse.wst.jsdt.core.dom.ASTVisitor;
import org.eclipse.wst.jsdt.core.dom.Assignment;
import org.eclipse.wst.jsdt.core.dom.Expression;
import org.eclipse.wst.jsdt.core.dom.FieldAccess;
import org.eclipse.wst.jsdt.core.dom.FunctionDeclaration;
import org.eclipse.wst.jsdt.core.dom.FunctionExpression;
import org.eclipse.wst.jsdt.core.dom.JavaScriptUnit;
import org.eclipse.wst.jsdt.core.dom.SimpleName;
import org.eclipse.wst.jsdt.core.dom.SingleVariableDeclaration;
import org.eclipse.wst.jsdt.core.dom.VariableDeclarationFragment;
import org.eclipse.wst.jsdt.internal.corext.SourceRange;

public class NJSDocOutlineNode implements ISourceReference {
	private String fSlotName;
	private NJSDocOutlineNode fParent;
	private final List<NJSDocOutlineNode> fChildren = new ArrayList<NJSDocOutlineNode>(0);
	private DocumentedSlot fSlot;
	private ASTNode fASTNode;
	/**
	 * Cached location hash
	 * 
	 * A value indicating the location of this function: The higher 16 bits are
     * the lexical depth, the lower 16 bits are the line number.
	 */
	private int fLocationHash = -1;
	/**
	 * Cached node name
	 */
	private String fName;
	/**
	 * May be null if in a different file
	 */
	private final String fFileName;
	private final JavaScriptUnit fRootAST;

	public NJSDocOutlineNode(JavaScriptUnit ast, String fileName, ASTNode node) {
		this.fASTNode = node;
		this.fFileName = fileName;
		this.fRootAST = ast;

		OutlineASTVisitor v = new OutlineASTVisitor(node);

		node.accept(v);
		for (ASTNode child : v.get()) {
			addChild(new NJSDocOutlineNode(ast, fileName, child));
		}
	}

	public NJSDocOutlineNode(JavaScriptUnit ast, String fileName, DocumentedSlot slot) {
		this.fSlot = slot;
		this.fFileName = fileName;
		this.fRootAST = ast;
	}

	public boolean isDescendentOf(NJSDocOutlineNode other) {
		NJSDocOutlineNode cur = this;
		while (cur != null) {
			if (cur == other) {
				return true;
			}
			assert cur.getParent() == null || cur.getParent().getChildren().contains(cur);
			cur = cur.getParent();
		}
		return other == null;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return (fASTNode != null ? "(L)" : "") + (fSlot != null ? "(R "+ getSlotName() +")" : "");
	}

	private void merge(NJSDocOutlineNode other) {
		assert other.fParent == null;
		for (NJSDocOutlineNode child : other.getChildren()) {
			child.fParent = null;
			addChild(child);
			assert fParent != child;
		}
		other.fChildren.clear();
	}

	public void mergeLexical(NJSDocOutlineNode other) {
		merge(other);
		assert fASTNode == null;
		fASTNode = other.fASTNode;
	}

	public void mergeRuntime(NJSDocOutlineNode other) {
		merge(other);
		assert fSlot == null;
		fSlot = other.fSlot;
	}

	protected int getLineNumber() {
		if (fSlot != null && fFileName != null) {
			CodeLocation best = null;
			for (CodeLocation loc : fSlot.getLocations()) {
				if (loc.file.equals(fFileName)) {
					if (best == null || best.line > loc.line) {
						best = loc;
					}
				}
			}
			if (best != null) {
				return best.line;
			}
		}

		if (fASTNode != null) {
			return fRootAST.getLineNumber(JavaEditor.getNodePos(fASTNode));
		}

		// Use child as an approximation
		int line = -1;

		for (NJSDocOutlineNode node : getChildren()) {
			line = node.getLineNumber();
			break;
		}

		return line;
	}

	public int getLocationHash() {
		if (fLocationHash  < 0) {
			if (fSlot != null && fFileName != null) {
				fLocationHash = fSlot.getValue().getLocationHash(); 
			} else if (fASTNode != null) {
				int depth = 0;
				NJSDocOutlineNode node = this;

				while (node != null) {
					if (node.fASTNode instanceof FunctionDeclaration) {
						depth++;
					}
					node = node.getParent();
				}

				fLocationHash = (depth << 16) + fRootAST.getLineNumber(JavaEditor.getNodePos(fASTNode));
			}
		}
		return fLocationHash;
	}

	public ASTNode getASTNode() {
		return fASTNode;
	}

	public void removeChild(NJSDocOutlineNode child) {
		assert child.fParent == this && fChildren.contains(child);
		fChildren.remove(child);
		child.fParent = null;
	}

	public NJSDocOutlineNode getParent() {
		return fParent;
	}

	public DocumentedSlot getSlot() {
		return fSlot;
	}
	
	public void addChild(NJSDocOutlineNode child) {
		assert child.fParent == null;
		child.fParent = this;
		fChildren.add(child);
	}

	public List<NJSDocOutlineNode> getChildren() {
		return fChildren;
	}

	public String getName() {
		if (fName == null) {
			if (fASTNode instanceof VariableDeclarationFragment) {
				fName = "var " + ((VariableDeclarationFragment) fASTNode).getName(); //$NON-NLS-1$
			} else if (fASTNode instanceof FunctionDeclaration) {
				FunctionDeclaration fd = (FunctionDeclaration) fASTNode;
				String name;

				if (fSlot == null) {
					if (fd.getName() != null) {
						name = fd.getName().toString();
					} else {
						name = "function"; //$NON-NLS-1$
					}
				} else {
					if (fd.getName() != null && hasSlotChild()) {
						// Heuristic to suppress --once behavior for non-members
						name = fd.getName().toString();
					} else {
						name = getSlotName();

						// Add more context to the node if parent slot context not known
						if (fParent != null && fParent.fSlot == null) {
							ASTNode parent = fd.getParent();

							if (parent instanceof FunctionExpression && (parent = parent.getParent()) instanceof Assignment) {
								Assignment expr = (Assignment) parent;
								String fieldName = getFieldFullName(expr.getLeftHandSide());

								if (fieldName != null && fieldName.endsWith(name)) {
									name = fieldName;
								}
							}
						}
					}
				}

				StringBuilder b = new StringBuilder(name);
				writeArgs(b, (fSlot == null || fSlot.getValue().getArgumentNames() == null) ? fd.parameters() : Arrays
						.asList(fSlot.getValue().getArgumentNames()));  

				fName = b.toString();
			} else if (fSlot != null) {
				StringBuilder b = new StringBuilder();
				b.append(getSlotName());
				
				switch (fSlot.getValue().getType()) {
				case FUNCTION:
				case METHOD:
				case CONSTRUCTOR:
					writeArgs(b, Arrays.asList((Object[])fSlot.getValue().getArgumentNames()));
				}
				fName = b.toString();
			}
		}
		return fName;
	}

	private String getFieldFullName(Expression expr) {
		if (expr instanceof SimpleName) {
			return expr.toString();
		}
		if (expr instanceof FieldAccess) {
			FieldAccess access = (FieldAccess) expr;
			String next = getFieldFullName(access.getExpression());
			if (next != null) {
				return next + "." + access.getName().toString(); //$NON-NLS-1$
			}
		}
		return null;
	}

	private boolean hasSlotChild() {
		for (NJSDocOutlineNode n : getChildren()) {
			if (n.fSlot != null) {
				return true;
			}
		}
		return false;
	}

	private String getSlotName() {
		return fSlotName != null ? fSlotName : fSlot.getName();
	}

	protected void setSlotName(String slotName) {
		fSlotName = slotName;
	}

	private void writeArgs(StringBuilder b, List<Object> col) {
		b.append('(');
		boolean first = true;

		for (Object o : col) {
			if (!first) {
				b.append(", "); //$NON-NLS-1$
			}
			first = false;
			b.append((o instanceof SingleVariableDeclaration) ? ((SingleVariableDeclaration)o).getName() : o.toString()); 
		}
		b.append(')');
	}

	/**
	 * @see org.eclipse.wst.jsdt.core.ISourceReference#exists()
	 */
	public boolean exists() {
		assert false;
		return true;
	}

	/**
	 * @see org.eclipse.wst.jsdt.core.ISourceReference#getSource()
	 */
	public String getSource() throws JavaScriptModelException {
		assert false;
		return null;
	}

	/**
	 * @see org.eclipse.wst.jsdt.core.ISourceReference#getSourceRange()
	 */
	public ISourceRange getSourceRange() throws JavaScriptModelException {
		SourceRange range = null;

		if (fASTNode instanceof FunctionDeclaration) {
			FunctionDeclaration fd = (FunctionDeclaration) fASTNode;						
			return new SourceRange((fd.getName() != null) ? fd.getName() : fASTNode);
		}

		if (fSlot != null) {
			int n = getLineNumber();

			if (n >= 0) {
				range = new SourceRange(fRootAST.getPosition(n, 1), 0);
			}
		}

		if (fASTNode != null) {
			SourceRange astRange = new SourceRange(fASTNode);

			if (range == null || astRange.covers(range)) {
				range = astRange;
			}
		}
		return range;
	}

	public boolean isAnonymousFunction() {
		return (getSlot() == null && fASTNode instanceof FunctionDeclaration && ((FunctionDeclaration) fASTNode)
				.getName() == null);
	}

	protected static Collection<NJSDocOutlineNode> getLexicalNodes(NJSDocOutlineNode rootOutline) {
		// The set of function declarations in the lexical tree - visit the lexical tree in BFS order
		Set<NJSDocOutlineNode> lexicalSet = new HashSet<NJSDocOutlineNode>();
		LinkedList<NJSDocOutlineNode> queue = new LinkedList<NJSDocOutlineNode>();
	    queue.add(rootOutline);
	    while (!queue.isEmpty()) {
	    	NJSDocOutlineNode curNode = queue.remove();
	    	if (curNode.getASTNode() instanceof FunctionDeclaration) {
	    		lexicalSet.add(curNode);
	    	}
	        queue.addAll(curNode.getChildren());
	    }
		return lexicalSet; 
	}

	/**
	 * Helper method to get roots nodes of the given collection.
	 */
	protected static Set<NJSDocOutlineNode> getRoots(Collection<NJSDocOutlineNode> nodes) {
		// 4. Find runtime subtree roots
		Set<NJSDocOutlineNode> roots = new TreeSet<NJSDocOutlineNode>(new Comparator());
	
		for(NJSDocOutlineNode node : nodes) {
			while (node.getParent() != null) {
				node = node.getParent();
			}
			roots.add(node);
		}
		return roots;
	}

	private static final class Comparator implements java.util.Comparator<NJSDocOutlineNode> {
		public int compare(NJSDocOutlineNode o1, NJSDocOutlineNode o2) {
			if (o1 == o2) {
				return 0;
			}
			int h1 = o1.getLineNumber(), h2 = o2.getLineNumber();
			if (h1 < 0) {
				h1 = (Integer.MAX_VALUE / 4);
			}
			if (h2 < 0) {
				h2 = (Integer.MAX_VALUE / 4);
			}
			int val = h1 - h2;
			if (val == 0) {
				val = o1.hashCode() - o2.hashCode();
				if (val == 0) {
					val = 1;
				}
			}
			return val;
		}
	}
	
	public static final class LocationHashComparator implements java.util.Comparator<NJSDocOutlineNode> {
		public int compare(NJSDocOutlineNode o1, NJSDocOutlineNode o2) {
			if (o1 == o2) {
				return 0;
			}
			int h1 = o1.getLocationHash(), h2 = o2.getLocationHash();
			if (h1 < 0) {
				h1 = (Integer.MAX_VALUE / 4);
			}
			if (h2 < 0) {
				h2 = (Integer.MAX_VALUE / 4);
			}
			int val = h1 - h2;
			if (val == 0) {
				val = o1.hashCode() - o2.hashCode();
				if (val == 0) {
					val = 1;
				}
			}
			return val;
		}
	}

	private static class OutlineASTVisitor extends ASTVisitor {
		private List<ASTNode> fItems = new ArrayList<ASTNode>(8);
		private ASTNode fNode;

		public OutlineASTVisitor(ASTNode node) {
			fNode = node;
		}

		/**
		 * @see org.eclipse.wst.jsdt.core.dom.ASTVisitor#visit(org.eclipse.wst.jsdt.core.dom.FunctionExpression)
		 */
		public boolean visit(FunctionDeclaration node) {
			if (node == fNode) {
				return true;
			}
			fItems.add(node);
			return false;
		}

		public ASTNode[] get() {
			return fItems.toArray(new ASTNode[fItems.size()]);
		}
	}
}