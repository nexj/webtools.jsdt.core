package org.eclipse.wst.jsdt.internal.njsdoc.model;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.wst.jsdt.core.IJavaScriptProject;
import org.eclipse.wst.jsdt.internal.core.Logger;

/**
 * @since 2.0
 */
public class NJSDocModel {
	private static final NJSDocModel model = new NJSDocModel();

	public static final String CONFIG_FILE_SUFFIX = "njsdoc"; //$NON-NLS-1$

	private final Object INVALID_PROJECT = new Object();

	/**
	 * Known project models. Null value indicates invalid configuration.
	 * 
	 * TODO: Listen for config file changes and invalidate/re-validate entries
	 * 
	 * TODO: use resource variables on project rather than static cache?
	 */
	private ConcurrentMap<IProject, Object> m_projects = new ConcurrentHashMap<IProject, Object>();

	private static ListenerList m_listeners = new ListenerList();

	private NJSDocModel() {
	}

	public static NJSDocModel getModel() {
		return model;
	}

	private static IFile getConfigFile(IProject project) {
		return project.getFile(".settings/." + CONFIG_FILE_SUFFIX);
	}

	public boolean isBuilt(IJavaScriptProject project) {
		return isBuilt(project.getProject());
	}

	public boolean isBuilt(IProject project) {
		return m_projects.get(project) instanceof ProjectModel;
	}

	public ProjectModel getProject(IProject project) {
		Object o = m_projects.get(project), o2;

		if (o == null) {
			try {
				o = m_projects.putIfAbsent(project, o2 = new ProjectModel(project, getConfigFile(project)));

				if (o == null) {
					o = o2;
				}
			} catch (CoreException e) {
				m_projects.putIfAbsent(project, INVALID_PROJECT);
				Logger.logException("Error loading NJSDoc configuration for project \"" + project.getName() + "\"", e);
				return null;
			}
		}

		return (ProjectModel) (o instanceof ProjectModel ? o : null);
	}

	public static void removeListener(NJSDocListener listener) {
		m_listeners.remove(listener);
	}

	public static void addListener(NJSDocListener listener) {
		m_listeners.add(listener);
	}

	/**
	 * Called by {@link ExecutionModel}s when they execute
	 * @param executionModel The model that just executed
	 */
	protected static void onExecute(ExecutionModel executionModel) {
		for (Object listener : m_listeners.getListeners()) {
			((NJSDocListener)listener).onExecute(executionModel);
		}
	}

	public interface NJSDocListener {
		public void onExecute(ExecutionModel m);
	}
}
