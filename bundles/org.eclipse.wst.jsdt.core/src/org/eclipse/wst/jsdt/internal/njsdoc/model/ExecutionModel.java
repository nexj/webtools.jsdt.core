package org.eclipse.wst.jsdt.internal.njsdoc.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.ExecutionListener;
import com.nexj.njsdoc.JSClass;
import com.nexj.njsdoc.NJSDoc;
import com.nexj.njsdoc.ProblemRequestor;
import com.nexj.njsdoc.SlotValue;
import com.nexj.njsdoc.SlotValue.Visitor;
import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;
import com.nexj.njsdoc.org.mozilla.javascript.Context;
import com.nexj.njsdoc.org.mozilla.javascript.Script;
import com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.wst.jsdt.core.IJavaScriptModelStatusConstants;
import org.eclipse.wst.jsdt.core.JavaScriptModelException;
import org.eclipse.wst.jsdt.internal.core.Logger;

/**
 * @since 2.0
 * 
 */
public class ExecutionModel {

	private static final int THIS_RESOLUTION_FUZZ = 20;

	private final String m_name;

	private SlotValue m_value;

	protected final ProjectModel m_parent;

	protected List<Step> m_steps = new ArrayList<Step>();

	public ExecutionModel(ProjectModel parent, String name) {
		m_parent = parent;
		m_name = name;
	}

	/**
	 * Blocks waiting for the value. If the execution model has never been
	 * executed, it is now executed.
	 * 
	 * Warning: may wait several hundred milliseconds
	 * 
	 * @return Not null
	 * @throws RuntimeException
	 *             If an error occurs
	 */
	public synchronized SlotValue getValue() {
		if (m_value == null) {
			execute();
		}

		return m_value;
	}

	public ProjectModel getParent() {
		return m_parent;
	}

	public void execute() {
		// If models were to maintain dependency graph and if primitive object
		// structure were retained per unit(?) then this could be overridden to
		// re-exec only minimal files.

		Context cx = Context.enter();
		ExecutionListener listener = new ExecutionListener();

		try {
			cx.setOptimizationLevel(-1);
			cx.addExecutionListener(listener);

			ScriptableObject scope = NJSDoc.makeScope(cx);
			long start = System.currentTimeMillis();

			// TODO: report NJSDoc problems
			m_value = execute(cx, scope, listener, ProblemRequestor.NULL_REQUESTOR);

			Logger.log(Logger.INFO, "Executing " + this + " took " + (System.currentTimeMillis() - start)
					+ "ms");
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			cx.removeExecutionListener(listener);
			// Exit from the context.
			Context.exit();
		}
		NJSDocModel.onExecute(this);
	}

	public boolean executionNeeded() {
		return m_value == null;
	}

	/**
	 * Attempt to resolve the JSClass for the this variable in the function
	 * identified by fileName and locationHash.
	 * 
	 * @param fileName
	 *            The file name to query
	 * @param locationHash
	 *            The location hash in the file
	 * @param cache
	 *            Transient cache for storing slots per file
	 * @return The class or null if unknown
	 */
	public JSClass resolveThis(String fileName, long locationHash, Map<String, List<SlotValue>> cache) {
		SlotValue value = getValue();
		String prefix = "/" + m_parent.getProject().getName() + "/";

		if (fileName.startsWith(prefix)) {
			fileName = fileName.substring(prefix.length());
		}

		final String relFileName = fileName;

		if (fileName == null || locationHash < 0) {
			return null;
		}

		List<SlotValue> candidates = (cache == null) ? null : cache.get(relFileName);

		if (candidates == null) {
			candidates = new ArrayList<SlotValue>(64);
			try {
				value.visitBF(new FileSlotSearcher(relFileName, candidates));
			} catch (Exception e) {
				return null;
			}
			((ArrayList<SlotValue>) candidates).trimToSize();
			cache.put(relFileName, candidates);
		}

		SlotValue best = null;

		for (SlotValue v : candidates) {
			long curDiff = Math.abs(v.getLocationHash() - locationHash);
			if (curDiff < THIS_RESOLUTION_FUZZ
					&& (best == null || (curDiff < Math.abs(best.getLocationHash() - locationHash)))) {
				best = v;
			}
		}

		if (best != null) {
			switch (best.getType()) {
			case CONSTRUCTOR:
				return best.getConstructorJSClass();
			case DATA:
				assert false;
				// $FALL-THROUGH$
			case FIELD:
				assert false;
				// $FALL-THROUGH$
			case FUNCTION:
				// This is a simple function - this is 'global'
				return null;
			case METHOD:
				return best.getJSClass();
			}
		}

		return null;
	}

	public void addFileStep(String fileName) throws JavaScriptModelException {
		m_steps.add(new FileStep(fileName));
	}

	public void addSequenceStep(String prefix, String fileName, Pattern linePattern) throws CoreException {
		IResource sequenceFile = m_parent.getProject().getWorkspace().getRoot().findMember(fileName);

		if (sequenceFile instanceof IFile) {
			m_steps.add(new SequenceStep(prefix, (IFile) sequenceFile, linePattern));
		} else {
			throw new JavaScriptModelException(new IllegalArgumentException(
					"NJSDoc: Invalid config format for project " + m_parent.getProject().getName()
							+ ": invalid sequence file: \"" + fileName + "\""),
					IJavaScriptModelStatusConstants.IO_EXCEPTION);
		}
	}

	/**
	 * @param path
	 *            Project relative path
	 * @param excludes
	 *            Paths relative to 'path'. May be null
	 */
	public void addModulesStep(String path, String[] excludes) throws JavaScriptModelException {
		IResource baseDir = m_parent.getProject().findMember(path);

		if (baseDir instanceof IContainer) {
			m_steps.add(new ModulesStep((IContainer) baseDir, excludes));
		} else {
			throw new JavaScriptModelException(new IllegalArgumentException(
					"NJSDoc: Invalid config format for project " + m_parent.getProject().getName()
							+ ": invalid modules path"), IJavaScriptModelStatusConstants.IO_EXCEPTION);
		}
	}

	public void addEvalStep(String js) {
		m_steps.add(new EvalStep(js));
	}

	public void makeStepsReadOnly() {
		m_steps = Collections.unmodifiableList(m_steps);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ExecutionModel#contains(org.eclipse.core.resources.IFile)
	 */
	public boolean contains(IFile file) {
		for (Iterator<Step> it = m_steps.iterator(); it.hasNext();) {
			if (it.next().contains(file)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ExecutionModel#execute(com.nexj.njsdoc.org.mozilla.javascript.Context,
	 *      com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject,
	 *      com.nexj.njsdoc.ExecutionListener)
	 */
	private SlotValue execute(Context cx, ScriptableObject scope, ExecutionListener listener, ProblemRequestor requestor)
			throws Exception {
		for (Iterator<Step> it = m_steps.iterator(); it.hasNext();) {
			it.next().execute(cx, scope, listener);
		}

		return NJSDoc.exec(listener, scope, true, requestor);
	}

	protected void execFile(Context cx, ScriptableObject scope, ExecutionListener listener, IFile file) {
		execScript(cx, scope, listener, m_parent.getBytecode(cx, file), file.getProjectRelativePath()
				.toString());
	}

	protected void execScript(Context cx, ScriptableObject scope, ExecutionListener listener, Script script,
			String fileName) {
		listener.startFile(fileName);

		if (script != null) {
			try {
				script.exec(cx, scope);
			} catch (Exception e) {
				Logger.log(Logger.INFO, "Error executing script while reconciling NJSDoc model", e);
			}
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if (m_name != null) {
			return "Recipe \"" + m_name + "\" for project \"" + m_parent.getProject() + "\"";
		}
		return super.toString();
	}

	// inner types

	protected abstract static class Step {

		public abstract boolean contains(IFile file);

		public abstract void execute(Context cx, ScriptableObject scope, ExecutionListener listener)
				throws CoreException;

	}

	private class EvalStep extends Step {
		private String m_js;
		private Script compiled;

		public EvalStep(String js) {
			m_js = js;
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#contains(org.eclipse.core.resources.IFile)
		 */
		public boolean contains(IFile file) {
			return false;
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#execute(com.nexj.njsdoc.org.mozilla.javascript.Context,
		 *      com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject,
		 *      com.nexj.njsdoc.ExecutionListener)
		 */
		public void execute(Context cx, ScriptableObject scope, ExecutionListener listener) {
			if (compiled == null) {
				compiled = cx.compileString(m_js, "<eval>", 1, null);
			}

			execScript(cx, scope, listener, compiled, "<eval>");
		}
	}

	private class SequenceStep extends Step {
		private final IFile m_sequenceFile;

		private final String m_prefix;

		private List<IFile> m_fileList;

		/**
		 * Optional line pattern. May be null.
		 */
		private final Pattern m_linePattern;

		public SequenceStep(String prefix, IFile sequenceFile, Pattern linePattern) throws CoreException {

			m_prefix = prefix == null ? "" : prefix;
			m_sequenceFile = sequenceFile;
			m_linePattern = linePattern;

			reloadFileList();
		}

		private void reloadFileList() throws CoreException {
			// TODO: proper exception instantiation
			m_fileList = new ArrayList<IFile>();

			List<String> sFiles;

			try {
				sFiles = NJSDoc.readSequenceFile(m_sequenceFile.getContents(), m_linePattern);
			} catch (IOException e) {
				throw new JavaScriptModelException(e, IJavaScriptModelStatusConstants.IO_EXCEPTION);
			}

			IResource res = m_parent.getProject().findMember(m_prefix);

			if (!(res instanceof IContainer)) {
				throw new JavaScriptModelException(new IllegalArgumentException("Bad resource path"),
						IJavaScriptModelStatusConstants.IO_EXCEPTION);
			}

			IContainer container = (IContainer) res;

			for (String curLine : sFiles) {
				IResource cur = container.findMember(curLine);

				if (cur instanceof IFile) {
					m_fileList.add((IFile) cur);
				} else {
					Logger.log(Logger.WARNING, toString() + " container invalid entry: " + curLine);
				}
			}
		}

		public String toString() {
			return "Sequence \"" + m_sequenceFile;
		}

		public boolean contains(IFile file) {
			return m_fileList.contains(file);
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#execute(com.nexj.njsdoc.org.mozilla.javascript.Context,
		 *      com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject,
		 *      com.nexj.njsdoc.ExecutionListener)
		 */
		public void execute(Context cx, ScriptableObject scope, ExecutionListener listener) {

			// Evaluate all the files
			for (Iterator<IFile> it = m_fileList.iterator(); it.hasNext();) {
				execFile(cx, scope, listener, it.next());
			}
		}
	}

	private class FileStep extends Step {
		private IFile m_file;

		public FileStep(String fileName) throws JavaScriptModelException {
			IResource file = m_parent.getProject().findMember(fileName);

			if (file instanceof IFile) {
				m_file = (IFile) file;
			} else {
				throw new JavaScriptModelException(new IllegalArgumentException("Couldn't resolve to file:"
						+ fileName), IJavaScriptModelStatusConstants.IO_EXCEPTION);
			}
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#contains(org.eclipse.core.resources.IFile)
		 */
		public boolean contains(IFile file) {
			return m_file.equals(file);
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#execute(com.nexj.njsdoc.org.mozilla.javascript.Context,
		 *      com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject,
		 *      com.nexj.njsdoc.ExecutionListener)
		 */
		public void execute(Context cx, ScriptableObject scope, ExecutionListener listener) {
			execFile(cx, scope, listener, m_file);
		}
	}

	private class ModulesStep extends Step {

		private final IContainer c;
		private final Set<String> excludes = new HashSet<String>();

		public ModulesStep(IContainer c, String[] excludes) {
			this.c = c;

			if (excludes != null) {
				for (String s : excludes) {
					this.excludes.add(s);
				}
			}
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#contains(org.eclipse.core.resources.IFile)
		 */
		public boolean contains(IFile file) {
			IPath fp, cp = c.getFullPath();

			return "js".equals(file.getFileExtension()) && cp.isPrefixOf(fp = file.getFullPath())
					&& !excludes.contains(fp.removeFirstSegments(cp.segmentCount()).toString());
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeExecutionModel.Step#execute(com.nexj.njsdoc.org.mozilla.javascript.Context,
		 *      com.nexj.njsdoc.org.mozilla.javascript.ScriptableObject,
		 *      com.nexj.njsdoc.ExecutionListener)
		 */
		public void execute(final Context cx, final ScriptableObject scope, final ExecutionListener listener)
				throws CoreException {
			// TODO: is visitation order deterministic?
			c.accept(new IResourceVisitor() {
				public boolean visit(IResource resource) throws CoreException {
					if (resource instanceof IFile && contains((IFile) resource)) {
						execFile(cx, scope, listener, (IFile) resource);
					}
					return true;
				}
			});
		}
	}

	// inner types

	/**
	 * Searches for slots contributed by the given file
	 */
	private final class FileSlotSearcher implements Visitor {

		private final String relFileName;
		private final List<SlotValue> candidates;

		private FileSlotSearcher(String relFileName, List<SlotValue> candidates) {
			this.relFileName = relFileName;
			this.candidates = candidates;
		}

		public boolean visit(DocumentedSlot doc) throws Exception {
			boolean found = false;

			for (CodeLocation loc : doc.getLocations()) {
				if (relFileName.equals(loc.file)) {
					found = true;
					break;
				}
			}

			if (found) {
				candidates.add(doc.getValue());
			}

			return true;
		}
	}

}
