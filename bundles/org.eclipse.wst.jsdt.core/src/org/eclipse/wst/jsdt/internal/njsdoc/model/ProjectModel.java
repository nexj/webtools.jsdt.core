package org.eclipse.wst.jsdt.internal.njsdoc.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

import com.nexj.njsdoc.ConfigAdapter;
import com.nexj.njsdoc.Pair;
import com.nexj.njsdoc.org.mozilla.javascript.Context;
import com.nexj.njsdoc.org.mozilla.javascript.Script;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.wst.jsdt.core.IJavaScriptModelStatusConstants;
import org.eclipse.wst.jsdt.core.JavaScriptModelException;
import org.eclipse.wst.jsdt.internal.core.Logger;
import org.mozilla.javascript.EcmaError;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.ScriptableObject;

/**
 * @since 2.0
 * 
 */
public class ProjectModel {

	private final IProject m_project;

	private volatile Collection<ExecutionModel> m_sequences;

	private final ConcurrentMap<IFile, Pair<Boolean, Script>> m_bytecode = new ConcurrentHashMap<IFile, Pair<Boolean, Script>>();

	private IFile m_configFile;

	public ProjectModel(IProject project, IFile configFile) throws CoreException {
		m_project = project;
		m_configFile = configFile;

		reloadConfiguration();
	}

	// operations

	public IProject getProject() {
		return m_project;
	}

	public void invalidateBytecode(IFile file) {
		Map<IFile, Pair<Boolean, Script>> bytecode = m_bytecode;
		Pair<Boolean, Script> el = bytecode.get(file);

		if (el != null) {
			synchronized (el) {
				el.head = Boolean.FALSE;
			}
		}
	}

	public void invalidateBytecode() {
		m_bytecode.clear();
	}

	public Script getBytecode(Context cx, IFile file) {
		Pair<Boolean, Script> entry = m_bytecode.get(file);

		if (entry == null) {
			Pair<Boolean, Script> oldEntry = m_bytecode.putIfAbsent(file, entry = new Pair<Boolean, Script>(
					Boolean.FALSE, null));

			if (oldEntry != null) {
				entry = oldEntry;
			}
		}

		synchronized (entry) {
			if (!entry.head.booleanValue()) {
				InputStream stream = null;
				InputStreamReader isr = null;
				try {
					stream = file.getContents();
					isr = new InputStreamReader(stream);
					entry.tail = cx.compileReader(isr, file.getProjectRelativePath().toString(), 1, null);
					entry.head = Boolean.TRUE;
				} catch (Exception e) {
					// Head remains false but if the bytecode was present before
					// it will remain - so that some functionality remains in
					// the presence of syntax errors
					// TODO: report error to the user as a problem(?)
					Logger.log(Logger.INFO, "Error compiling code for NJSDoc", e); //$NON-NLS-1$
				} finally {
					try {
						if (stream != null) {
							stream.close();
						}
						if (isr != null) {
							isr.close();
						}
					}
					catch (IOException e) {
						Logger.log(Logger.ERROR, "Error closing streams while compiling code for NJSDoc", e); //$NON-NLS-1$
					}
				}
			}
		}

		// May be null or out of date
		return entry.tail;
	}

	public Collection<ExecutionModel> getExecutionModels() {
		return Collections.unmodifiableCollection(m_sequences);
	}

	/**
	 * Get execution models relevant to the given file
	 * 
	 * @param res The resource to check
	 * @return Execution models that this file belongs to. May be empty
	 */
	public Collection<ExecutionModel> getExecutionModels(IFile res) {
		List<ExecutionModel> l = new ArrayList<ExecutionModel>(m_sequences.size());

		for (ExecutionModel m : m_sequences) {
			if (m.contains(res)) {
				l.add(m);
			}
		}

		return l;
	}

	/**
	 * @return The configuration file that defines the recipes/sequences in use.
	 */
	public IFile getConfigFile() {
		return m_configFile;
	}

	/**
	 * TODO: use xml configuration?
	 */
	public void reloadConfiguration() throws CoreException {
		m_sequences = new ArrayList<ExecutionModel>();

		if (!m_configFile.exists()) {
			return;
		}

		org.mozilla.javascript.Context cx = org.mozilla.javascript.Context.enter();
		InputStream reader = null;
		InputStreamReader stream = null;

		try {
			cx.setOptimizationLevel(-1);
			org.mozilla.javascript.Scriptable scope = cx.initStandardObjects();

			Object adapter = org.mozilla.javascript.Context.javaToJS(new JSAdapter(), scope);

			org.mozilla.javascript.ScriptableObject.putProperty(scope, "JSDT", adapter);
			org.mozilla.javascript.ScriptableObject.putProperty(scope, "NJSDoc", adapter);

			reader = m_configFile.getContents();
			stream = new InputStreamReader(reader);

			cx.evaluateReader(scope, stream, m_configFile.getName(), 0, null);
		} catch (IOException e) {
			throw new JavaScriptModelException(e, IJavaScriptModelStatusConstants.IO_EXCEPTION);
		} catch (EcmaError e) {
			throw new JavaScriptModelException(e, IJavaScriptModelStatusConstants.IO_EXCEPTION);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (stream != null) {
					stream.close();
				}
			} catch (IOException e) {
				Logger.log(Logger.ERROR, "Error closing configuration file", e); //$NON-NLS-1$
			}

			org.mozilla.javascript.Context.exit();
		}
	}

	// inner types

	/**
	 * Exposed to JavaScript. Public so Rhino will import the members.
	 */
	public class JSAdapter implements ConfigAdapter {
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ConfigAdapter#defineSequence(java.lang.String, java.lang.String)
		 */
		public void defineSequence(String prefix, String fileName) throws CoreException {
			m_sequences.add(new SequenceExecutionModel(ProjectModel.this, prefix, fileName, null));
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ConfigAdapter#defineSequence(java.lang.String, java.lang.String, java.lang.String)
		 */
		public void defineSequence(String prefix, String fileName, String regex) throws CoreException {
			m_sequences.add(new SequenceExecutionModel(ProjectModel.this, prefix, fileName, Pattern.compile(regex)));
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ConfigAdapter#define()
		 */
		public void define() throws CoreException {
			m_sequences.add(new ModuleExecutionModel(ProjectModel.this));
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ConfigAdapter#recipe()
		 */
		public RecipeAdapter recipe() {
			return recipe("#" + m_sequences.size());
		}

		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.ConfigAdapter#recipe(java.lang.String)
		 */
		public RecipeAdapter recipe(String name) {
			ExecutionModel recipe = new ExecutionModel(ProjectModel.this, name);

			m_sequences.add(recipe);

			return new RecipeAdapter(recipe);
		}
	}

	public static class RecipeAdapter implements com.nexj.njsdoc.RecipeAdapter {
		private final ExecutionModel model;

		public RecipeAdapter(ExecutionModel model) {
			this.model = model;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#file(java.lang.String)
		 */
		public RecipeAdapter file(String fileName) throws JavaScriptModelException {
			model.addFileStep(fileName);
			return this;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#sequence(java.lang.String, java.lang.String)
		 */
		public RecipeAdapter sequence(String prefix, String fileName) throws CoreException {
			model.addSequenceStep(prefix, fileName, null);
			return this;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#sequence(java.lang.String, java.lang.String, java.lang.String)
		 */
		public RecipeAdapter sequence(String prefix, String fileName, String regex) throws CoreException {
			model.addSequenceStep(prefix, fileName, Pattern.compile(regex));
			return this;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#modules(java.lang.String)
		 */
		public RecipeAdapter modules(Object options) throws CoreException {
			String path = "";
            String[] excludes = null;

            if (options instanceof String) {
                path = (String) options;
            } else if (options instanceof ScriptableObject) {
                ScriptableObject so = (ScriptableObject) options;

                if (ScriptableObject.hasProperty(so, "path")) {
                    Object t = ScriptableObject.getProperty(so, "path");
                    if (t instanceof String) {
						path = (String) t;
					} else {
						throw new JavaScriptModelException(new IllegalArgumentException(
								"NJSDoc: Invalid config format for project "
										+ model.getParent().getProject().getName()
										+ ": modules() expected 'path' to be a string"),
								IJavaScriptModelStatusConstants.IO_EXCEPTION);
					}
                }
                
                if (ScriptableObject.hasProperty(so, "exclude")) {
                	Object t = ScriptableObject.getProperty(so, "exclude");
                    if (t instanceof String) {
                        excludes = new String[] {(String)t};
                    } else if (t instanceof NativeArray) {
                        NativeArray na = (NativeArray) t;
                        List<String> pathList = new ArrayList<String>();

                        for (int i = 0, n = (int)na.getLength(); i < n; i++) {
                            t = ScriptableObject.getProperty(na, i);
                            
                            if (t instanceof String) {
                                pathList.add((String) t);
							} else {
								throw new JavaScriptModelException(
										new IllegalArgumentException(
												"NJSDoc: Invalid config format for project "
														+ model.getParent().getProject().getName()
														+ ": modules() expected 'exclude' to contain a string or an array of strings"),
										IJavaScriptModelStatusConstants.IO_EXCEPTION);
							}
						}

						excludes = pathList.toArray(new String[pathList.size()]);
					} else {
						throw new JavaScriptModelException(
								new IllegalArgumentException(
										"NJSDoc: Invalid config format for project "
												+ model.getParent().getProject().getName()
												+ ": modules() expected 'exclude' to contain a string or an array of strings"),
								IJavaScriptModelStatusConstants.IO_EXCEPTION);
					}
				}
            }

			model.addModulesStep(path, excludes);
			return this;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#eval(java.lang.String)
		 */
		public RecipeAdapter eval(String js) {
			model.addEvalStep(js);
			return this;
		}
		
		/**
		 * @see org.eclipse.wst.jsdt.internal.njsdoc.model.RecipeAdapter#define()
		 */
		public void define() {
			model.makeStepsReadOnly();
		}
	}
}
