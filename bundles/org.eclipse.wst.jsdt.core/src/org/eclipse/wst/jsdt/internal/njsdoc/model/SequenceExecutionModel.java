package org.eclipse.wst.jsdt.internal.njsdoc.model;

import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;

/**
 * @since 2.0
 */
public class SequenceExecutionModel extends ExecutionModel {

    public SequenceExecutionModel(ProjectModel parent, String prefix, String sequenceFile, Pattern linePattern)
            throws CoreException {
        super(parent, null);
        addSequenceStep(prefix, sequenceFile, linePattern);
        makeStepsReadOnly();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Sequence \"" + m_steps.get(0).toString() + "\" for project \"" + m_parent.getProject() + "\"";
    }
}
