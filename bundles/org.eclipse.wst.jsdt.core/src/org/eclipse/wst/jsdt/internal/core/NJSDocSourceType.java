package org.eclipse.wst.jsdt.internal.core;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.ProblemRequestor;
import com.nexj.njsdoc.SlotValue;
import com.nexj.njsdoc.export.HTMLDocumentationExporter;
import com.nexj.njsdoc.export.Variable;
import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;

import org.eclipse.core.resources.IContainer;
import org.eclipse.wst.jsdt.internal.compiler.lookup.NJSDocBinding;
import org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement;

/**
 * 
 */
public class NJSDocSourceType extends SourceType implements IDocumentedJavaScriptElement  {

	private final NJSDocBinding binding;

	public NJSDocSourceType(JavaElement parent, NJSDocBinding binding) {
		super(parent, new String(binding.sourceName));
		this.binding = binding;
	}

	public NJSDocBinding getBinding() {
		return binding;
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLabel()
	 */
	public String getLabel() {
		DocumentedSlot slot = binding.getSlot();
		SlotValue value = slot.getValue();
		StringWriter sw = new StringWriter();
		BufferedWriter br = new BufferedWriter(sw);
		
		try {
			switch(value.getType()) {
			case CONSTRUCTOR:
				new HTMLDocumentationExporter(br).writeConstructorTitle(value.getConstructorJSClass());
				break;
			case FUNCTION:
				//$FALL-THROUGH$
			case METHOD:
				// Occurs if a method is stored in a structure
				//$FALL-THROUGH$
			default:
				new HTMLDocumentationExporter(br).writeVariableTitle(new Variable(name, slot,
						ProblemRequestor.NULL_REQUESTOR));
				break;
			}

			br.flush();
		} catch (IOException e1) {
			return null;
		}

		return sw.toString();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getDetailHTML()
	 */
	public String getDetailHTML() {
		DocumentedSlot slot = binding.getSlot();
		SlotValue value = slot.getValue();
		StringWriter sw = new StringWriter();
		BufferedWriter br = new BufferedWriter(sw);

		try {
			switch(value.getType()) {
			case CONSTRUCTOR:
				new HTMLDocumentationExporter(br).writeConstructorDetail(value.getConstructorJSClass());
				break;
			case FUNCTION:
				//$FALL-THROUGH$
			case METHOD:
				// Function stored in a field
				//$FALL-THROUGH$
			default:
				new HTMLDocumentationExporter(br).writeVariableDetail(new Variable(name, slot,
						ProblemRequestor.NULL_REQUESTOR));
				break;
			}

			br.flush();
		} catch (IOException e1) {
			return null;
		}

		return sw.toString();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLocations()
	 */
	public Collection<CodeLocation> getLocations() {
		return binding.getSlot().getLocations();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getBaseResource()
	 */
	public IContainer getBaseResource() {
		return binding.getBaseResource();
	}
}
