package org.eclipse.wst.jsdt.core;

import java.util.List;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.JSClass;
import com.nexj.njsdoc.ProblemRequestor;
import com.nexj.njsdoc.export.DocumentationExporter.Argument;
import com.nexj.njsdoc.export.Function;

import org.eclipse.wst.jsdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocLabelFormatter;

/**
 * @since 2.0
 */
public class NJSDocCompletionProposal extends CompletionProposal {

	private DocumentedSlot slot;
	private JSClass jsClass;

	public NJSDocCompletionProposal(int kind, int completionLocation) {
		super(kind, completionLocation);
	}

	public void setSlot(String name, DocumentedSlot slot) {
		setSlot(null, name, slot, false);
	}

	public void setSlot(JSClass jsClass, String name, DocumentedSlot slot, boolean isVariable) {
		setName(name.toCharArray());
		this.slot = slot;
		this.jsClass = jsClass;

		int flags = 0;

		if (slot.getValue().getType().isStatic() && !isVariable) {
			flags |= ClassFileConstants.AccStatic;
		}

		switch (slot.getValue().getType()) {
		case FUNCTION:
		case CONSTRUCTOR:
		case METHOD:
			flags |= ClassFileConstants.AccPublic;
			initWithFunction(name, slot);
			break;
		case FIELD:
			flags |= ClassFileConstants.AccDefault;
			break;
		default:
			flags |= ClassFileConstants.AccPublic;
		}

		setCompletion(NJSDocLabelFormatter
				.getLabel(jsClass, new String(getName()), slot, NJSDocLabelFormatter.Flavor.VERY_CONCISE).toCharArray());

		setFlags(flags);

		if (getKind() == CompletionProposal.TYPE_REF) {
			setSignature(("" + Signature.C_RESOLVED + name).toCharArray());
		}
	}

	private void initWithFunction(String name, DocumentedSlot slot) {
		Function function = new Function(name, slot, ProblemRequestor.NULL_REQUESTOR);
		List<Argument> arguments = function.getArguments();
		char[][] names = new char[arguments.size()][];

		for (int i = 0; i < names.length; i++) {
			names[i] = arguments.get(i).getName().toCharArray();
		}

		setParameterNames(names);
	}

	public String getHTMLDoc() {
		String sName = new String(getName());
		StringBuilder buf = new StringBuilder();
		buf.append("<h5>");
		buf.append(NJSDocLabelFormatter.getLabel(jsClass, sName, slot, NJSDocLabelFormatter.Flavor.FULL));
		buf.append("</h5>");
		buf.append(NJSDocLabelFormatter.getDetail(jsClass, sName, slot));
		return buf.toString();
	}

	public String getLabel() {
		return NJSDocLabelFormatter.getLabel(jsClass, new String(getName()), slot, NJSDocLabelFormatter.Flavor.CONCISE);
	}

	/**
	 * 
	 */
	public String getQualifiedTypeName() {
		switch (getKind()) {
		case CompletionProposal.TYPE_REF:
			return new String(getName());
		default:
			assert false;
		}
		
		return null;
	}
}
