# NJSDoc for JSDT #

This is fork of JSDT integrating [NJSDoc](https://bitbucket.org/nexj/njsdoc). 

# Status #

* Type resolution for other files is using NJSDoc as its data source 
* Content assist uses NJSDoc
* Hover Doc uses NJSDoc
* Jump to definition uses NJSDoc
* Outline view showing hybrid of lexical and NJSDoc structure
* Screenshots [here](http://imgur.com/a/bT4Cd)

To try out the features on this fork on JSDT you can clone this [Eclipse project](https://bitbucket.org/nexj/njsdoc-demo).

See also this on the Eclipse BugZilla about merging to JSDT: [Investigate merging NJSDoc support into JSDT](https://bugs.eclipse.org/bugs/show_bug.cgi?id=406575)

# Update site #

JSDT and NJSDoc are available on this update site: 

    http://devtools.nexj.com/updatesite/ 
	
# Configuration #

The first step is to configure your .project file to enable JSDT and the NJSDoc builder

    <?xml version="1.0" encoding="UTF-8"?>
    <projectDescription>
    	<name>..</name>
    	<buildSpec>
    		<!-- For be performance remove org.eclipse.wst.jsdt.core.javascriptValidator -->
    		<!-- Add this -->
    		<buildCommand>
    			<name>org.eclipse.wst.jsdt.core.njsdocBuilder</name>
    			<arguments>
    			</arguments> 
    		</buildCommand>
    	</buildSpec>
    	<natures>
    		<nature>org.eclipse.wst.jsdt.core.jsNature</nature>
    	</natures>
    </projectDescription>

Next add configuration so that NJSDoc knows the relationship between the Javascript files in your project. NJSDoc uses this information to execute JavaScript 
in the appropriate order to build the data structures that drive content assist. Create a file under '.settings' folder called '.njsdoc'. It's contents are 
JavaScript. Each expression specifies an execution model, there can be multiple per project. Compatible with NJSDoc --recipe files. For example:

	// Advanced arbitrary recipe with files, sequence files and evaluated code
	// file(), sequence(), and eval() can each be included zero or more times
    NJSDoc.recipe("mobile web")
	  .file("external/jquery.js") // project relative file path
	  //.sequence(<base path>, <workspace relative path to file contaning list of paths one per line>, [optional line filter regex]);
	  .sequence("src/", "otherProjectName/src/../files.lst")
	  .sequence("src/", "otherProjectName/application.hta", "<script.*src=\"(.*)\\?")
	  .eval("var g = new MyGlobalObject()") // arbitrary expressions
	  .define(); // complete the definition

    // If execution order doesn't matter use modules()
    NJSDoc.recipe("amd based library")
      .file("src/sys.js")
      .modules({path: "src/", exclude: ["src/end.js", "src/sys.js"]})
      .file("src/end.js");

# Build #

* Clone the standard JSDT repositories
* Clone this Mercurial repository: https://bitbucket.org/nexj/njsdoc-build and import this Eclipse project into your workspace
* For webtools.jsdt.core repository get the code from here (https://bitbucket.org/nexj/webtools.jsdt.core) and switch to the njsdoc branch

	  

